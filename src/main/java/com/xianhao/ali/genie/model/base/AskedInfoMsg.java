package com.xianhao.ali.genie.model.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xianhao
 */
@Data
public class AskedInfoMsg implements Serializable {

    /**
     * 询问的参数名（非实体名）
     */
    private String parameterName;
    /**
     * 意图 ID，从请求参数中获得
     */
    private Long intentId;
}
