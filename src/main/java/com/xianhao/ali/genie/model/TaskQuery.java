package com.xianhao.ali.genie.model;

import com.xianhao.ali.genie.model.base.SlotEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 请求字段
 * @author xianhao
 */
@Data
public class TaskQuery implements Serializable {

    /**
     * 会话 ID
     * session 内的对话，此 ID 相同
     */
    private String sessionId;
    /**
     * 请求 ID
     */
    private String requestId; // 文档中未体现的字段，实际存在
    /**
     * 应用 ID
     * 来自创建的应用或者技能
     */
    private Long botId;
    /**
     * 用户的输入语句
     */
    private String utterance;
    /**
     * 技能 ID
     */
    private Long skillId;
    /**
     * 技能名称
     */
    private String skillName;
    /**
     * 意图 ID
     */
    private Long intentId;
    /**
     * 意图名称
     */
    private String intentName;
    /**
     * 技能授权 token，非必需
     */
    private String token;
    /**
     * 业务请求附带参数
     * 主要信息字段：eg.
     *  city:String:放置设备所处的城市信息
     */
    private Map<String, String> requestData;
    /**
     * 从用户语句中抽取出的 slot 参数信息
     */
    private List<SlotEntity> slotEntities;
    /**
     * 领域 ID
     */
    private Long domainId;
}
