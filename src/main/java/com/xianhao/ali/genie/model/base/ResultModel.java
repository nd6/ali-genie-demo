package com.xianhao.ali.genie.model.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 响应模型
 * @param <T> 意图结果详情
 * @author xianhao
 */
@Data
public class ResultModel<T> implements Serializable {
    /**
     * 返回状态码
     * “0” 默认标识成功，其他为不成功的字段
     */
    private String returnCode = "0";
    /**
     * 出错时解决方法的描述信息
     */
    private String returnErrorSolution;
    /**
     * 返回执行成功的描述信息
     */
    private String returnMessage;
    /**
     * 意图理解后的执行结果
     */
    private T returnValue;
}
