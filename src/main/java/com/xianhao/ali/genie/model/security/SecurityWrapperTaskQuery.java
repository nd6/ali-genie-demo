package com.xianhao.ali.genie.model.security;

import lombok.Data;

import java.io.Serializable;

/**
 * 加密包装的查询参数
 * @author xianhao
 */
@Data
public class SecurityWrapperTaskQuery implements Serializable {

    /**
     * 加密请求元数据
     */
    private QueryMetaInfo queryMetaInfo;
    /**
     * 将 TaskQuery 加密后的字符串
     */
    private String securityQuery;
}
