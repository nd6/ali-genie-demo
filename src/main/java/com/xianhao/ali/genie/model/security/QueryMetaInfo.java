package com.xianhao.ali.genie.model.security;

import lombok.Data;

import java.io.Serializable;

/**
 * 查询元数据信息
 * @author xianhao
 */
@Data
public class QueryMetaInfo implements Serializable {

    /**
     * 请求来源标示
     */
    private String source;
    /**
     * 加密策略版本
     */
    private String queryVersion;
    /**
     * 加密方式，当前为 RSA
     */
    private String securityMode;
}
