package com.xianhao.ali.genie.model.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author xianhao
 */
@Data
public class Action implements Serializable {

    /**
     * 名称（必须设置为 "audioPlayGenieSource"）
     */
    private String name;
    /**
     * 信息字段
     * "audioGenieId" 的 key 必须设置，标示播放的开放平台存储的音频 ID
     */
    private Map properties;
}
