package com.xianhao.ali.genie.model;

import com.xianhao.ali.genie.model.base.Action;
import com.xianhao.ali.genie.model.base.AskedInfoMsg;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author xianhao
 */
@Data
public class TaskResult implements Serializable {

    /**
     * 回复播报语句
     */
    private String reply;
    /**
     * 回复时的状态标识
     * <p>
     *  ASK_INF：信息获取
     *  RESULT：正常完成交互的阶段并给出回复
     *  CONFIRM：期待确认
     * </p>
     */
    private String resultType;
    /**
     * 生成回复语句时携带的额外信息
     */
    private Map<String, String> properties;
    /**
     * 在 ASK_INF 状态下，必须设置本次追问的具体参数名
     * （开发者平台意图参数下配置的参数信息）
     */
    private List<AskedInfoMsg> askedInfos;
    /**
     * 播控类信息
     * ！！！目前只支持播放音频
     */
    private List<Action> actions;
    /**
     * <p>
     *     SUCCESS：执行成功
     *     PARAMS_ERROR：代表接收到的请求参数出错
     *     EXECUTE_ERROR：代表自身代码有异常
     *     REPLY_ERROR：代表回复结果生成出错
     * </p>
     */
    private String executeCode;
}
