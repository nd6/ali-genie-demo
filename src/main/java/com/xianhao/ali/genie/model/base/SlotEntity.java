package com.xianhao.ali.genie.model.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 词槽实体
 * @author xianhao
 */
@Data
public class SlotEntity implements Serializable {

    /**
     * 意图参数 ID
     */
    private Long intentParameterId;
    /**
     * 意图参数名称
     */
    private String intentParameterName;
    /**
     * 原始句子中抽取出来的未做处理的 slot 值
     */
    private String originalValue;
    /**
     * 原始 slot 归一化后的值
     */
    private String standardValue;
    /**
     * 该 slot 已生存的时间（会话轮数）
     */
    private Integer liveTime;
    /**
     * 产生的时间点
     */
    private Long createTimeStamp;
    /**
     * slot 值
     */
    private String slotValue; // 文档中未体现的字段，实际存在
}
