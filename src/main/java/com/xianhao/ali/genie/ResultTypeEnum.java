package com.xianhao.ali.genie;

/**
 * 回复状态标识
 * @author xianhao
 */
public enum ResultTypeEnum {

    ASK_INF("ASK_INF"), // 信息获取
    RESULT("RESULT"), // 正常完成交互的阶段并给出回复
    CONFIRM("CONFIRM"); // 期待确认

    private String type;

    ResultTypeEnum(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
