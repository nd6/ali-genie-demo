package com.xianhao.ali.genie;

/**
 * 执行结果编码
 * @author xianhao
 */
public enum ExecuteCodeEnum {

    SUCCESS("SUCCESS", "执行成功"),
    PARAMS_ERROR("PARAMS_ERROR", "接收到的请求参数出错"),
    EXECUTE_ERROR("EXECUTE_ERROR", "代码有异常"),
    REPLY_ERROR("REPLY_ERROR", "结果生成出错");

    private String code;
    private String message;

    ExecuteCodeEnum(String code, String message){
        this.code = code;
        this.message = message;
    }

    public static String message(String code){
        for (ExecuteCodeEnum e : ExecuteCodeEnum.values()){
            if(e.getCode().equals(code)){
                return e.getMessage();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
