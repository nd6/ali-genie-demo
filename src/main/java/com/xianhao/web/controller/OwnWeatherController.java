package com.xianhao.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.xianhao.ali.genie.model.base.ResultModel;
import com.xianhao.ali.genie.model.TaskQuery;
import com.xianhao.ali.genie.model.TaskResult;
import com.xianhao.handler.WeatherHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class OwnWeatherController {

    @Autowired
    private WeatherHandle weatherHandle;

    @PostMapping(value = "skill/weather")
    private ResultModel<TaskResult> testResponse(@RequestBody String taskQuery){

        log.info("TaskQuery: {}", taskQuery);
        TaskQuery query = JSONObject.parseObject(taskQuery, TaskQuery.class);
        log.info("数据解析成功, {}", query);

        // 构建返回结果
        ResultModel<TaskResult> resultModel = new ResultModel<>();

        try {
            // 假装调用了真实的处理，嘿嘿
            TaskResult result = weatherHandle.execute(query);
            resultModel.setReturnCode("0");
            resultModel.setReturnValue(result);
        } catch (Exception e){
            resultModel.setReturnCode("-1");
            resultModel.setReturnErrorSolution(e.getMessage());
        }
        log.info("返回结果，{}", resultModel);
        return resultModel;
    }
}
