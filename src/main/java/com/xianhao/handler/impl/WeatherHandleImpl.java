package com.xianhao.handler.impl;

import com.xianhao.ali.genie.ExecuteCodeEnum;
import com.xianhao.ali.genie.ResultTypeEnum;
import com.xianhao.ali.genie.model.TaskQuery;
import com.xianhao.ali.genie.model.TaskResult;
import com.xianhao.handler.WeatherHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WeatherHandleImpl implements WeatherHandle {

    @Override
    public TaskResult execute(TaskQuery taskQuery) {

        log.info("{}.execute", this.getClass().getSimpleName());

        // 从请求中获取意图参数以及参数值
        Map<String, String> paramMap = taskQuery.getSlotEntities()
                .stream()
                .collect(Collectors.toMap(
                        slotEntity -> slotEntity.getIntentParameterName(),
                        slotEntity -> slotEntity.getStandardValue()));

        if("天气查询".equals(taskQuery.getIntentName())){
            return baseQuery(taskQuery, paramMap);
        }

        return null;
    }

    private TaskResult baseQuery(TaskQuery taskQuery, Map<String, String> paramMap){

        TaskResult result = new TaskResult();

        try {
            // 假装真的处理了
            if(paramMap == null || paramMap.size() < 1){
                result.setExecuteCode(ExecuteCodeEnum.PARAMS_ERROR.getCode());
                result.setReply("输入出错啦");
            } else {
                result.setExecuteCode(ExecuteCodeEnum.SUCCESS.getCode());
                result.setReply(paramMap.get("city") + " " + paramMap.get("time") + " 天气棒棒哒！！");
            }
        } catch (Exception e){
            log.error("query exception {}", e);
            result.setExecuteCode(ExecuteCodeEnum.EXECUTE_ERROR.getCode());
        }

        result.setResultType(ResultTypeEnum.RESULT.getType());
        return result;
    }
}
