package com.xianhao.handler;

import com.xianhao.ali.genie.model.TaskQuery;
import com.xianhao.ali.genie.model.TaskResult;

/**
 * 天气处理
 * @author xianhao
 */
public interface WeatherHandle {

    /**
     * 执行函数
     * @param taskQuery 查询参数
     * @return 响应结果
     */
    TaskResult execute(TaskQuery taskQuery);
}
